% plot simulated spectrum (figure 1b)
clearvars;
if ~exist('first','var')
    run('custom_settings')
    load([getenv('datadir') 'simulated'],'X','gendata');
    first = 1;
end

fig = opt.fig;
fig.size = 90*fig.col2width*[1 1/1.48];

[N,W] = size(X);

figure;
clf
[maxA maxinx] = max(gendata.A);
plot(X(maxinx,:))
hold on
plot(gendata.A(maxinx)*voigt(gendata.theta,1:W),'--')
ylabel(['Intensity'])
xlabel(['Wavenumber'])
xlim([1 W])
prepfig([getenv('figdir') 'simulated_spectrum'],fig)