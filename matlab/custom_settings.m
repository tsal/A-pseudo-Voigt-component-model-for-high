% This file will set default parameters for the matlab scripts
%
% Variables that must be defined by this script:
%   opt.fig    : struct that is passed to prepfig
%   strFIGdir  : directory that contains figures
%   strDATAdir : directory that contains data
%   strMATdir  : directory to store mat files

% Copyright (c) 2012, Tommy Sonne Alstrøm, Technical University of Denmark

% add lib files
path(pathdef);
addpath(genpath(fullfile(pwd,'lib')));
clear opt;

fDebug = true;
if( fDebug )
    rng('default')
end

% prepfig settings
opt.fig.col1width = 7.0;% width of figures with 1 column
opt.fig.col2width = 3.7;% width of figures with 2 column 
opt.fig.keepTextColor = true;
opt.fig.afs = 8;
opt.fig.fs = 8;
opt.fig.font = 'Arial';
opt.fig.Interpreter = 'latex';
opt.fig.saveFragToDisk = false;
opt.fig.box = true;
opt.fig.hires = 300;
opt.fig.lwg = 1;

% sge settings
opt.usesge = ~ispc;
opt.usegpu = gpuDeviceCount > 0;

% directory definitions
strMATdir = [fullfile(pwd,'..','data') filesep];
strFIGdir = [fullfile(pwd,'..','figures') filesep];
strDATAdir = [fullfile(pwd,'..','data') filesep];

setenv('matdir', strMATdir);
setenv('datadir', strDATAdir);
setenv('figdir', strFIGdir);

% check that dir exists
[o1 o2] = mkdir(strMATdir);
[o1 o2] = mkdir(strFIGdir);