function prepfig(filename,opt)
%prepfig
% Prepares current figure and prints png

% This software is intended for research and educational purposes. Any sale
% or commercial distribution is strictly forbidden, unless the Department
% of Mathematical Modelling, Tecnical University of Denmark has given
% explicit permission.
% 
% The software is provided "as-is". Support is generally not available.
% No warranties of any kind, express or implied, are made as to it or any 
% medium it may be on. No remedy will be provided for indirect, 
% consequential, punitve or incidental damages arising from it, including
% such from negligence, strict liability, or breach of warranty or
% contract, even after notice of the possibility of such damages.
% Copyright (c)
% Jan Larsen, DTU Compute, 2003-2009
% Tommy Sonne Alstrøm, DTU Compute, 2011-2017

% backwards compability
% opt used to be a binary variable, invert
if( exist('opt','var') )
    if( ~isa(opt,'struct') )
        opt_.invert = opt;
        opt = opt_;
        clear opt_;
    end
else
    opt.invert = false;
    opt.afs = 16;
end

% axis font size; integer, op
p = inputParser;
%p.StructExpand = true;
p.KeepUnmatched = true;
p.FunctionName = 'prepfig';
%axis font size
p.addOptional('afs', 16, @(x)validateattributes(x, ...
    {'numeric'}, {'scalar', 'integer', 'positive'}));
%label font size
p.addOptional('fs', 19, @(x)validateattributes(x, ...
    {'numeric'}, {'scalar', 'integer', 'positive'}));
%line width edge
p.addOptional('lwe', 1, @(x)validateattributes(x, ...
    {'numeric'}, {'scalar', 'integer', 'positive'}));
%line width graph
p.addOptional('lwg', 2, @(x)validateattributes(x, ...
    {'numeric'}, {'scalar', 'integer', 'positive'}));
%line width axis
p.addOptional('lwa', 0.5, @(x)validateattributes(x, ...
    {'numeric'}, {'scalar', 'positive'}));
%invert
p.addOptional('invert', false, @(x)validateattributes(x, ...
    {'numeric','logical'}, {'scalar', 'binary'}));
%save images to disk
p.addOptional('saveToDisk', true, @(x)validateattributes(x, ...
    {'numeric','logical'}, {'scalar', 'binary'}));
%save eps to disk
p.addOptional('saveEPSToDisk', true, @(x)validateattributes(x, ...
    {'numeric','logical'}, {'scalar', 'binary'}));
%save matlabfrag to disk
p.addOptional('saveFragToDisk', false, @(x)validateattributes(x, ...
    {'numeric','logical'}, {'scalar', 'binary'}));
%Colors
p.addOptional('BackgroundColor', [1 1 1], @(x)validateattributes(x, ...
    {'numeric'}, {'>=',0,'<=',1,'size',[1,3]}));
p.addOptional('TextColor', [0 0 0], @(x)validateattributes(x, ...
    {'numeric'}, {'>=',0,'<=',1,'size',[1,3]}));
%save eps to disk
p.addOptional('hires', false, @(x)validateattributes(x, ...
    {'numeric','logical'}, {'scalar', 'nonnegative'}));
%font name
p.addOptional('font', 'Helvetica', @ischar);
%font name
p.addOptional('Interpreter', 'tex', @ischar);
% figure dimensionality
p.addOptional('size', [], @(x)validateattributes(x, ...
    {'numeric'}, {'>=',0,'integer','size',[1,2]}));
% keep text colors
p.addOptional('keepTextColor', false, @(x)validateattributes(x, ...
    {'numeric','logical'}, {'scalar', 'binary'}));
% keep all props
p.addOptional('setProps', true, @(x)validateattributes(x, ...
    {'numeric','logical'}, {'scalar', 'binary'}));
% delete text
p.addOptional('noText', false, @(x)validateattributes(x, ...
    {'numeric','logical'}, {'scalar', 'binary'}));
% box the plot
p.addOptional('box', true, @(x)validateattributes(x, ...
    {'numeric','logical'}, {'scalar', 'binary'}));

% need to remake opt to cell, otherwise the error report mechanism in
% matlab does not work
vals = struct2cell(opt);
names = fieldnames(opt);
k = 1;
for i=1:length(vals)
    optcell{k} = names{i};
    optcell{k+1} = vals{i};
    k = k + 2;
end
p.parse(optcell{:});
opt = p.Results;

% some helper strings, this is a false/true to off on string
onoff = {'off','on'};
opt.box = onoff{opt.box+1};

% todo
%inverted color
bgcolori=[0 0 0];
tcolori=[1 1 1];
% normal color
bgcolorn=opt.BackgroundColor;
tcolorn=opt.TextColor;


% process input
if nargin==0
   opt.invert=0;
   filename='';
end
if nargin==1
   if isstr(filename)
     opt.invert=0;
   else
     opt.invert=filename;
     filename='';
   end
end
if( isempty(filename) )
    filename='';
end
% backwards compability
if( opt.hires==1 )
    opt.hires = 300;
end

% check that matlabfrag exist
if( opt.saveFragToDisk & ~exist('matlabfrag') )
    warning('saveFragToDisk is true but matlabfrag does not exist');
    opt.saveFragToDisk = false;
end

if ( opt.saveToDisk &&  ~strcmp(filename,'') )
    % print original files first
%    print( gcf,'-dpng','-r300',[filename,'_org.png']);
%    print( gcf,'-depsc2',[filename,'_org.eps']);
%    matlabfrag([filename,'_frag']);
end

if opt.invert~=0
    set(gcf,'inverthardcopy','on');
    bgcolor=bgcolori;
    tcolor=tcolori;
else
    set(gcf,'inverthardcopy','off');
    bgcolor=bgcolorn;
    tcolor=tcolorn;
end

if( opt.setProps )
    set(gcf,'color',bgcolor);
    hf=get(gcf,'Children');
    ha=get(gca,'Children');
    % find hggroups
    t = findobj(gca,'Type','hggroup');
    ch = [];
    for i=1:length(t)
        ch = [ch; get(t(i),'Children')];
    end
    %dbstop if caught error
    hf = [hf; ch; ha];
    for i=1:length(hf)
        % handle each element seperately
        setProps(hf(i), opt);
    end
    %dbclear if caught error
    %for i=1:length(ha)
    %    try set(ha(i),'FontName',opt.font,'FontSize',opt.fs,'LineWidth',opt.lwg); end
    %end
end

% set figure size
if( ~isempty(opt.size) )
    pos = get(gcf,'Position');
    set(gcf,'Position',[pos(1:2) opt.size]);
    set(gcf,'PaperPositionMode','auto')
end

if ( opt.saveToDisk && ~strcmp(filename,'') )
  if( opt.hires )
    resolution = ['-r' num2str(opt.hires)];
  else
    resolution = '-r90';
  end
  if opt.invert~=0
      print( gcf,'-dpng',resolution,[filename,'_inv.png']);
    else
      print( gcf,'-dpng',resolution,[filename,'.png']);
  end
end

if ( opt.saveEPSToDisk && ~strcmp(filename,'') )
  if opt.invert~=0
      print( gcf,'-depsc2',[filename,'_inv.eps']);
    else
      print( gcf,'-depsc2',[filename,'.eps']);
  end
end

if ( opt.saveFragToDisk && ~strcmp(filename,'') )
  if opt.invert~=0
      matlabfrag([filename,'_frag_inv']);
    else
      matlabfrag([filename,'_frag']);
  end
end

if ( ~strcmp(filename,'') )
    if opt.invert~=0
      print( gcf,'-dpdf',[filename,'_inv.pdf']);
    else
      print( gcf,'-dpdf',[filename,'.pdf']);
  end
end

function setProps(hf, opt)
    type = get(hf,'Type');
    switch(type)
        case{'text'}
            if( opt.noText ) 
                delete(hf);
                return;
            end
            try set(hf,'Interpreter',opt.Interpreter); end
            if( ~opt.keepTextColor )
                try set(hf,'Color',opt.TextColor); end
            end
            if( strcmp(opt.Interpreter,'latex') )
                ob = get(hf,'String');
                set(hf,'String',strrep(ob,'-','--'));
            end
            try set(hf,'FontName',opt.font,'FontSize',opt.afs); end
        case{'line'}
            try set(hf,'LineWidth',opt.lwg); end
        case{'axes'}
            if( opt.noText ) 
                set(get(hf,'Title'),'String','');
                set(get(hf,'Xlabel'),'String','');
                set(get(hf,'Ylabel'),'String','');
                set(get(hf,'Zlabel'),'String','');
                set(hf,'XTickLabel',[]);
                set(hf,'YTickLabel',[]);
                set(hf,'ZTickLabel',[]);
                return;
            end
            try set(get(hf,'Xlabel'),'FontName',opt.font,'FontSize',opt.fs); end
            try set(get(hf,'Ylabel'),'FontName',opt.font,'FontSize',opt.fs); end
            try set(get(hf,'Zlabel'),'FontName',opt.font,'FontSize',opt.fs); end
            try set(get(hf,'Title'),'FontName',opt.font,'FontSize',opt.fs); end
            try set(get(hf,'Xlabel'),'Interpreter',opt.Interpreter); end
            try set(get(hf,'Ylabel'),'Interpreter',opt.Interpreter); end
            try set(get(hf,'Zlabel'),'Interpreter',opt.Interpreter); end
            try set(get(hf,'Title'),'Interpreter',opt.Interpreter); end
            if( ~opt.keepTextColor )
                try set(get(hf,'Xlabel'),'Color',opt.TextColor); end
                try set(get(hf,'Ylabel'),'Color',opt.TextColor); end
                try set(get(hf,'Zlabel'),'Color',opt.TextColor); end
                try set(get(hf,'Title'),'Color',opt.TextColor); end
                try set(hf,'XColor',opt.TextColor); end
                try set(hf,'YColor',opt.TextColor); end
                try set(hf,'ZColor',opt.TextColor); end
            end
            try set(hf,'FontSize',opt.afs); end
            try set(hf,'LineWidth',opt.lwa); end
            try set(hf,'Box',opt.box); end
        case{'patch','hggroup','image','uicontextmenu','uimenu','uitoolbar', ...
                'surface'}
            % do nothing
        otherwise
            try set(get(hf,'Xlabel'),'FontName',opt.font,'FontSize',opt.fs); end
            try set(get(hf,'Ylabel'),'FontName',opt.font,'FontSize',opt.fs); end
            try set(get(hf,'Zlabel'),'FontName',opt.font,'FontSize',opt.fs); end
            try set(get(hf,'Title'),'FontName',opt.font,'FontSize',opt.fs); end
            try set(hf,'Color',bgcolor); end
            if( ~opt.keepTextColor )
                try set(get(hf,'Xlabel'),'Color',opt.TextColor); end
                try set(get(hf,'Ylabel'),'Color',opt.TextColor); end
                try set(get(hf,'Zlabel'),'Color',opt.TextColor); end
                try set(get(hf,'Title'),'Color',opt.TextColor); end
                try set(hf,'XColor',opt.TextColor); end
                try set(hf,'YColor',opt.TextColor); end
                try set(hf,'ZColor',opt.TextColor); end
            end
            try set(hf,'FontSize',opt.afs); end
            try set(hf,'LineWidth',opt.lwe); end
            try hfc=get(hf,'Children');
            if ~isempty(hfc)
                if ((length(hfc)>=3) && (rem(length(hfc),3)==0))
                %set(hf,'TextColor',tcolor);
                %set(hf,'EdgeColor',tcolor);
                end
            end
        end
        hfc = get(hf,'Children');
        for i=1:length(hfc)
            setProps(hfc(i), opt);
        end
    end
