clearvars;
if ~exist('first','var')
    run('custom_settings')
    load([strMATdir 'simulated'])
    first = 1;
end

M = 10000;
Mest = 1000;

rng(1)
K=2;
[W N]=size(X');
[Am,Bm,sigmam] = gibbsnmf(X',K,M, ...
    'alpha',10, ...
    'beta', 1);

save([strMATdir 'simulated_nmf.mat'] , ...
    'Am','Bm','sigmam')