% plot simulated data (figure 1a)
clearvars;
if ~exist('first','var')
    run('custom_settings')
    load([getenv('datadir') 'simulated'],'X','gendata');
    first = 1;
end

fig = opt.fig;
fig.size = 90*fig.col2width*[1 1/1.48];

[N,W] = size(X);
figure
clf
imagesc(1:W,1:N,X)
colorbar
xlabel(['Wavenumber'])
ylabel(['Measurement index'])
prepfig([getenv('figdir') 'simulated_data'],fig)