# Main settings, fill these out
MAINFILE=icassp2017
PAPERTITLE=A pseudo-Voigt component model for high-resolution recovery of constituent spectra in Raman spectroscopy
FIGURES=simulated_data.pdf simulated_spectrum.pdf simulated_recovered_spectrum.pdf \
simulated_background.pdf simulated_loadings.pdf

# Directories setup
FIGDIR=figures
LATEXDIR=latex
MATDIR=matlab
VPATH=$(FIGDIR):$(LATEXDIR):$(MATDIR)

# Commands to run
PS2PDF=ps2pdf
DVI2PS=dvips
MATLAB=matlab -nodisplay -nosplash -nodesktop -wait -r
PDFCROP=pdfcrop

EMPTY=
SPACE=$(EMPTY) $(EMPTY)
TARGETPDF=$(subst $(SPACE),\ ,$(PAPERTITLE)).pdf

all: $(TARGETPDF)

$(TARGETPDF): figures $(LATEXDIR)/$(MAINFILE).pdf
	cp "$(LATEXDIR)/$(MAINFILE).pdf" "$(PAPERTITLE).pdf"

figures: $(FIGURES)

figures-crop: figures $(subst .pdf,-crop.pdf,$(FIGURES))

clean: clean-tmp
	-rm -f "$(PAPERTITLE).pdf"
	-rm -f $(FIGDIR)/*

clean-tmp: clean-tex
	-rm -f $(FIGDIR)/*-org.pdf
	-rm -f $(FIGDIR)/*-crop.pdf
 
clean-tex:
	-rm -f $(LATEXDIR)/*.bbl
	-rm -f $(LATEXDIR)/*.log
	-rm -f $(LATEXDIR)/*.blg
	-rm -f $(LATEXDIR)/*.dvi
	-rm -f $(LATEXDIR)/*.ps
	-rm -f $(LATEXDIR)/*.tex~
	-rm -f $(LATEXDIR)/*.out
	-rm -f $(LATEXDIR)/$(MAINFILE).pdf

# generate pdf figure file from m file
%-org.pdf : plot_%.m
	cd $(MATDIR) && $(MATLAB) "plot_$*;quit"
	mv $(FIGDIR)/$*.pdf $(FIGDIR)/$*-org.pdf 

# crop a pdf file
%.pdf : %-org.pdf 
	$(PDFCROP) $(FIGDIR)/$*-org.pdf
	mv $(FIGDIR)/$*-org-crop.pdf $(FIGDIR)/$*.pdf

%-crop.pdf : %.pdf 
	$(PDFCROP) $(FIGDIR)/$*.pdf
	mv $(FIGDIR)/$*.pdf $(FIGDIR)/$*-org.pdf
	cp -a $(FIGDIR)/$*-crop.pdf $(FIGDIR)/$*.pdf
	# need to touch crop file to avoid make running recipe again
	touch $(FIGDIR)/$*-crop.pdf

%.dvi : %.tex
	cd $(<D) && latex $(<F)
	cd $(<D) && bibtex $(*F).aux
	# run latex again to get references right
	cd $(<D) && latex $(<F)

%.pdf : %.tex
	# run pdflatex to create aux file
	cd $(<D) && pdflatex $(<F)
	cd $(<D) && bibtex $(*F).aux
	# run pdflatex twice to get references right
	cd $(<D) && pdflatex $(<F)
	cd $(<D) && pdflatex $(<F)

%.pdf : %.ps
	$(PS2PDF) $< 

%.ps : %.dvi
	$(DVI2PS) $< -o $@