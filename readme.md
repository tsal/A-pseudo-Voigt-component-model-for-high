# Content
This repository contains all the code and data files to reproduce the paper "A pseudo-Voigt component model for high-resolution recovery of constituent spectra in Raman spectroscopy". The main contribution of the paper namely a source seperation using a pseudo-Voigt component model is implemented in the file `gibbsvoigt.m`

This repository is maintained by Tommy Sonne Alstrøm, for any issues feel free to contact me at tsal@dtu.dk

## Copyright
The paper itself is copyright 2017 IEEE. Published in the IEEE 2017 International Conference on Acoustics, Speech, and Signal Processing (ICASSP 2017), scheduled for 5-9 March 2017 in New Orleans, Louisiana, USA. Personal use of this material is permitted. However, permission to reprint/republish this material for advertising or promotional purposes or for creating new collective works for resale or redistribution to servers or lists, or to reuse any copyrighted component of this work in other works, must be obtained from the IEEE. Contact: Manager, Copyrights and Permissions / IEEE Service Center / 445 Hoes Lane / P.O. Box 1331 / Piscataway, NJ 08855-1331, USA. Telephone: + Intl. 908-562-3966.

## Cloning
The repository can be either [downloaded](https://archive.compute.dtu.dk/archives/public/papers/2017/A-pseudo-Voigt-component-model-for-high.zip) in it's entirety directly from web or be cloned as a git reposity using 
```sh
# git clone git@lab.compute.dtu.dk:tsal/A-pseudo-Voigt-component-model-for-high.git
```
The git repository contains everything **excluding** data which  must be downloaded seperately from the [DTU compute archive](https://archive.compute.dtu.dk/archives/public/papers/2017/A-pseudo-Voigt-component-model-for-high/data.zip).

A gitlab webpage of the repository can be found [here](https://lab.compute.dtu.dk/tsal/A-pseudo-Voigt-component-model-for-high).

## Reproduction
Simulations and figures has been created with Matlab 2016a on Windows10 64bit. To reproduce the paper figures and pdf open a terminal and use
```sh
# make clean all
```
The makefile has been tested on Ubuntu linux and on windows 10 bash build 1607. Previous builds of windows 10 bash are not supported by the makefile. Mac OS should work as well. The makefile makes use of pdfcrop so make sure that is installed. This is found in the debian package texlive-extra-utils.

The simulations are not reproduced by the makefile. These can be reproduced by running the two matlab scripts manually
```matlab
>> run_nmf
>> run_voigt
```
The corresponding mat files will not be 100% identical since the mat files are a result of a simuation. Even with the random number generator started the same way, the operating system and versions of software libraries will alter the results slightly. 

## Expanded derivations of the samling expressions
To appear
